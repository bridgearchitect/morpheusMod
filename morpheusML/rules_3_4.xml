<?xml version="1.0" encoding="UTF-8"?>

<Ruleset version-from="3" version-to="4">
	<!--- The global Field part -->
	<Rule severty="info" operation="move">
		<Description>Rename AddCell/Condition to Count</Description>
		<MatchPath>/MorpheusModel/CellTypes/CellType/AddCell/Condition</MatchPath>
		<TargetPath>/MorpheusModel/CellTypes/CellType/AddCell/Count</TargetPath>
		<Message>Condition of AddCell component is renamed to Count in v4</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> Introduce vector notations </Description>
		<MatchPath>
			/MorpheusModel/*/VectorConstant/@spherical
		</MatchPath>
		<TargetPath>
			/MorpheusModel/*/VectorConstant/@notation
		</TargetPath>
		<ConversionMap>
			<Value from="false" to="x,y,z" />
			<Value from="true" to="φ,θ,r" />
		</ConversionMap>
		<Message>
			Notation of vector values is given in @notation attribute in v4.
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> Introduce vector notations </Description>
		<MatchPath>
			/MorpheusModel/*/VectorVariable/@spherical
		</MatchPath>
		<TargetPath>
			/MorpheusModel/*/VectorVariable/@notation
		</TargetPath>
		<ConversionMap>
			<Value from="false" to="x,y,z" />
			<Value from="true" to="φ,θ,r" />
		</ConversionMap>
		<Message>
			Notation of vector values is given in @notation attribute in v4.
		</Message>
	</Rule>
	
	<Rule severty="silent" operation="move">
		<Description> Adapt Event trigger values</Description>
		<MatchPath>
			/MorpheusModel/*/Event/@trigger
		</MatchPath>
		<TargetPath>
			/MorpheusModel/*/Event/@trigger
		</TargetPath>
		<ConversionMap>
			<Value from="when true" to="when-true" />
			<Value from="on change" to="on-change" />
		</ConversionMap>
		<Message>
			Notation of vector values is given in @notation attribute in v4.
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> Move Diffusion into Systems </Description>
		<MatchPath>
			/MorpheusModel/*/VectorProperty/@spherical
		</MatchPath>
		<TargetPath>
			/MorpheusModel/*/VectorProperty/@notation
		</TargetPath>
		<ConversionMap>
			<Value from="false" to="x,y,z" />
			<Value from="true" to="φ,θ,r" />
		</ConversionMap>
		<Message>
			Notation of vector values is given in @notation attribute in v4.
		</Message>
	</Rule>

	<Rule severty="info" operation="move">
		<Description> Move Diffusion into Systems </Description>
		<MatchPath>
			/MorpheusModel/*/VectorEquation/@spherical
		</MatchPath>
		<TargetPath>
			/MorpheusModel/*/VectorEquation/@notation
		</TargetPath>
		<ConversionMap>
			<Value from="false" to="x,y,z" />
			<Value from="true" to="φ,θ,r" />
		</ConversionMap>
		<Message>
			Notation of vector values is given in @notation attribute in v4.
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> Move Diffusion into Systems </Description>
		<MatchPath>
			/MorpheusModel/*/VectorRule/@spherical
		</MatchPath>
		<TargetPath>
			/MorpheusModel/*/VectorRule/@notation
		</TargetPath>
		<ConversionMap>
			<Value from="false" to="x,y,z" />
			<Value from="true" to="φ,θ,r" />
		</ConversionMap>
		<Message>
			Notation of vector values is given in @notation attribute in v4.
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> Move Diffusion into Systems </Description>
		<MatchPath>
			/MorpheusModel/*/InitVectorProperty/@spherical
		</MatchPath>
		<TargetPath>
			/MorpheusModel/*/InitVectorProperty/@notation
		</TargetPath>
		<ConversionMap>
			<Value from="false" to="x,y,z" />
			<Value from="true" to="φ,θ,r" />
		</ConversionMap>
		<Message>
			Notation of vector values is given in @notation attribute in v4.
		</Message>
	</Rule>
	
	
	<Rule severty="silent" operation="move">
		<Description> Solver renaming </Description>
		<MatchPath>
			/MorpheusModel/*/System/@solver
		</MatchPath>
		<TargetPath>
			/MorpheusModel/*/System/@solver
		</TargetPath>
		<ConversionMap>
			<Value from="euler" to="Euler [fixed, O(1)]" />
			<Value from="fixed1" to="Euler [fixed, O(1)]" />
			<Value from="heun" to="Heun [fixed, O(2)]" />
			<Value from="fixed2" to="Heun [fixed, O(2)]" />
			<Value from="runge" to="Runge-Kutta [fixed, O(4)]" />
			<Value from="runge-kutta" to="Runge-Kutta [fixed, O(4)]" />
			<Value from="fixed4" to="Runge-Kutta [fixed, O(4)]" />
			<Value from="adaptive45" to="Dormand-Prince [adaptive, O(5)]" />
			<Value from="adaptive45-dp" to="Dormand-Prince [adaptive, O(5)]" />
			<Value from="adaptive45-ck" to="Cash-Karp [adaptive, O(5)]" />
			<Value from="adaptive23" to="Bogacki-Shampine [adaptive, O(3)]" />
			<Value from="stochastic" to="Euler-Maruyama [stochastic, O(1)]" />
		</ConversionMap>
		<Message>
			Solvers were renamed in v4.
		</Message>
	</Rule>
	
	
	<Rule severty="info" operation="move">
		<Description> Move Gnuplotter z-slice to Plot level </Description>
		<MatchPath>
			/MorpheusModel/Analysis/Gnuplotter/Plot/Field/@slice
		</MatchPath>
		<TargetPath>
			/MorpheusModel/Analysis/Gnuplotter/Plot/@slice
		</TargetPath>
		<Message>
			Gnuplotter z-slice was moved to Plot level.
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> Move Gnuplotter z-slice to Plot level </Description>
		<MatchPath>
			/MorpheusModel/Analysis/Gnuplotter/Plot/Cells/@slice
		</MatchPath>
		<TargetPath>
			/MorpheusModel/Analysis/Gnuplotter/Plot/@slice
		</TargetPath>
		<Message>
			Gnuplotter z-slice was moved to Plot level.
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> Move Gnuplotter opacity to Cells level </Description>
		<MatchPath>
			/MorpheusModel/Analysis/Gnuplotter/Terminal/@opacity
		</MatchPath>
		<TargetPath>
			/MorpheusModel/Analysis/Gnuplotter/Plot/Cells/@opacity
		</TargetPath>
		<Message>
			Gnuplotter opacity was moved to Cells level.
		</Message>
	</Rule>
	
	<Rule severty="silent" operation="move">
		<Description> DependencyGraph renamed to ModelGraph </Description>
		<MatchPath>
			/MorpheusModel/Analysis/DependencyGraph
		</MatchPath>
		<TargetPath>
			/MorpheusModel/Analysis/ModelGraph
		</TargetPath>
		<Message>
			DependencyGraph was renamed to ModelGraph
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> CellReporter renamed to Mapper </Description>
		<MatchPath>/MorpheusModel/CellTypes/CellType/CellReporter</MatchPath>
		<TargetPath>/MorpheusModel/CellTypes/CellType/Mapper</TargetPath>
		<Message>CellReporter was renamed to Mapper in v4</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> ShapeBoundary renamed to ShapeSurface </Description>
		<MatchPath>/MorpheusModel/CPM/ShapeBoundary</MatchPath>
		<TargetPath>/MorpheusModel/CPM/ShapeSurface</TargetPath>
		<Message>ShapeBoundary was renamed to ShapeSurface in v4</Message>
	</Rule>
	
	
	<Rule severty="silent" operation="move">
		<Description> Value attribute renamed </Description>
		<MatchPath>
			/MorpheusModel/Analysis/Gnuplotter/Plot/CellLabels/@symbol-ref
		</MatchPath>
		<TargetPath>
			/MorpheusModel/Analysis/Gnuplotter/Plot/CellLabels/@value
		</TargetPath>
		<Message>
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> Moved Logger's force-node-granularity attribute </Description>
		<MatchPath>
			/MorpheusModel/Analysis/Logger/Restriction/@force-node-granularity
		</MatchPath>
		<TargetPath>
			/MorpheusModel/Analysis/Logger/Input/@force-node-granularity
		</TargetPath>
		<Message>
			 Logger's force-node-granularity attribute was moved in v4.
		</Message>
	</Rule>
	
	<Rule severty="silent" operation="move">
		<Description> Move Diffusion into Systems </Description>
		<MatchPath>
			/MorpheusModel/CellPopulations/Population/Cell/@name
		</MatchPath>
		<TargetPath>
			/MorpheusModel/CellPopulations/Population/Cell/@id
		</TargetPath>
		<Message>
		</Message>
	</Rule>
	
	<Rule severty="silent" operation="move">
		<Description> Move Diffusion into Systems </Description>
		<MatchPath>
			/MorpheusModel/CellPopulations/Population/Cell/@name
		</MatchPath>
		<TargetPath>
			/MorpheusModel/CellPopulations/Population/Cell/@id
		</TargetPath>
		<Message>
		</Message>
	</Rule>
	
	<Rule severty="info" operation="move">
		<Description> InitCellObject cleanup </Description>
		<MatchPath>
			/MorpheusModel/CellPopulations/Population/InitCellObjects/Arrangement/Object/*
		</MatchPath>
		<TargetPath>
			/MorpheusModel/CellPopulations/Population/InitCellObjects/Arrangement/*
		</TargetPath>
		<Message>
			InitCellObject cleanup
		</Message>
	</Rule>
	
	<Rule severty="silent" operation="delete">
		<Description> InitCellObject cleanup </Description>
		<MatchPath>
			/MorpheusModel/CellPopulations/Population/InitCellObjects/Arrangement/Object
		</MatchPath>
		<TargetPath></TargetPath>
		<Message>
			InitCellObject cleanup
		</Message>
	</Rule>
	
	
</Ruleset>
