#include "parallel_thread_pool.h"
#include <iostream>


ParallelThreadPool::ParallelThreadPool(int pool_size) : done(false) {
	setSize(pool_size);
}
	
int ParallelThreadPool::setSize(unsigned int pool_size) {
	if (local_queues.size() == pool_size) return pool_size;
	if (local_queues.empty()) {
		done = false;
		local_queues.resize(pool_size);
		for (auto& queue : local_queues) {
			queue = new SuspendableQueue();
			threads.push_back(std::thread([this, queue]() { worker_loop(queue); } ));
		}
		return pool_size;
	}
	else if (pool_size == 0) {
		std::cout << "Releasing thread pool!" << std::endl;
		done = true;
		for (auto& queue: local_queues) {
			queue->queue_cv.notify_all();
		}
		for (auto& thread : threads) {
			thread.join();
		}
		threads.clear();
		for (auto& queue: local_queues) {
			delete queue;
		}
		local_queues.clear();
		return 0;
	}
	else {
		throw ("Thread pool already initialized");
		return 0;
	}
};

ParallelThreadPool::~ParallelThreadPool() { 
	setSize(0);
}


std::vector<std::thread::id> ParallelThreadPool::getThreadIds() const {
	std::vector<std::thread::id> ids;
	for (const auto& thread : threads) {
		ids.push_back(thread.get_id());
	}
	return ids;
}
	
void ParallelThreadPool::worker_loop(SuspendableQueue* local_queue) {
	while (1) {
		std::unique_lock<std::mutex> latch(local_queue->queue_mtx);
		local_queue->queue_cv.wait(latch, [this, local_queue](){ return done || !local_queue->queue.empty() /*||  !global_queue.empty()*/; });
		
		if (done) {
// 			std::cout << "Finishing worker loop! " << std::this_thread::get_id() << std::endl;
			return;
		}
		
		if (!local_queue->queue.empty()) {
			pool_task task( local_queue->queue.front() );
			local_queue->queue.pop();
			task();
		}
// 		else if (global_queue.try_pop(task)) {
// 			task();
// 		}
	}
}
