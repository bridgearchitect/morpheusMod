#pragma once
#include <memory>
#include <thread>
#include <functional>
#include <future>
#include <condition_variable>
#include <queue>

template <class T>
class threadsafe_queue {
	std::queue<T> data;
	mutable std::mutex mtx;
	std::condition_variable data_cond;
	
public:
	threadsafe_queue() = default;
	threadsafe_queue(const threadsafe_queue<T>& other) {
		std::lock_guard lock(other.mtx);
		data = other.data;
	}
	const threadsafe_queue<T>& operator=(const threadsafe_queue<T>& other) {
		std::lock_guard lock(other.mtx);
		data = other.data;
		return *this;
	}
	bool empty() const {
		return data.empty();
	}
	void push(T&& value) {
		std::lock_guard lock(mtx);
		data.emplace( std::move(value) );
		data_cond.notify_one();
	}
	void push(const T& value) {
		std::lock_guard lock(mtx);
		data.push( value );
		data_cond.notify_one();
	}
	
	bool try_pop(T& t) {
		std::lock_guard lock(mtx);
		if ( data.empty() ) return false;
		t = data.front();
		data.pop();
		return true;
	}
	
	void wait_for_pop(T& t) {
		std::unique_lock lock(mtx);
		data_cond.wait(lock, [this] { return !data.empty();});
		t = data.front();
		data.pop();
	}
	
};

class anonymous_task {
	
	struct impl_base {
		virtual void call() const  =0;
		virtual ~impl_base() {}
	};
	
	template <class F>
	struct impl_deriv : impl_base {
		impl_deriv(F&& f) : func(std::move(f)) {};
		void call() const override { func(); }
		mutable F func;
	};
	
	std::unique_ptr<impl_base> impl;

public:
	template <class F>
	anonymous_task(F&& f) : impl(new impl_deriv(std::move(f)) ) {}
	anonymous_task(anonymous_task&& other) : impl( std::move(other.impl) ) {}
	anonymous_task& operator=(anonymous_task&& other) = default;
	
	void operator()() { if (impl) impl->call(); }
};

using pool_task = anonymous_task;

// NumberedThreadPool allows to submit a task to a specific thread
class ParallelThreadPool {
public:
	ParallelThreadPool() = default;
	ParallelThreadPool(int pool_size);
	~ParallelThreadPool();
	
	// assigns tasks to one thread each
	template<class F, class A>
	std::vector< std::future<typename std::result_of<F(A)>::type > > parallel_submit(F func, const std::vector<A>& args)
	{
		std::vector< std::future<typename std::result_of<F(A)>::type > > results;
		int n_threads = args.size();
		for (int t=0; t<n_threads; t++) {
			results.push_back( std::move(submit(func, args[t], threads.size()-1-t)) );
		}
		
		return results;
	}
	
	template<class F, class A>
	std::future<typename std::result_of<F(A)>::type >  submit(F func, const A& arg, int pool_id=-1)
	{
		using result_type =  typename std::result_of<F(A)>::type;
		std::packaged_task< result_type() > task( std::move(std::bind(func, arg))  );
		std::future<result_type> result = task.get_future();
		
		if (pool_id>=0 && pool_id<local_queues.size()) {
			{
				std::unique_lock<std::mutex> latch(local_queues[pool_id]->queue_mtx);
				local_queues[pool_id]->queue.push(std::move(task));
			}
			local_queues[pool_id]->queue_cv.notify_one();
		}
		else {
			global_queue.push(std::move(task));
		}
		return result;
	}
	
	int size() const { return local_queues.size(); }
	int setSize(unsigned int size);
	
	std::vector<std::thread::id> getThreadIds() const;
	
private:
	std::atomic_bool done;
	std::vector<std::thread> threads;
	threadsafe_queue<pool_task> global_queue;
	struct SuspendableQueue {
		std::queue<pool_task> queue;
		std::condition_variable queue_cv;
		std::mutex queue_mtx;
		
	};
	std::vector<SuspendableQueue*> local_queues;
	
	void worker_loop(SuspendableQueue* local_queue);
};
