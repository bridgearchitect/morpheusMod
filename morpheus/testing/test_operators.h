#include "core/vec.h"
#include "gtest/gtest.h"
#include "eigen/Eigen/Eigen"
#include <boost/math/distributions/normal.hpp>

::testing::AssertionResult EQ_PREC(const char* m_expr, const char* n_expr, double m, double n);

::testing::AssertionResult EQ_PREC(const char* m_expr, const char* n_expr, float  m, float n);

::testing::AssertionResult EQ_PREC(const char* m_expr, const char* n_expr, VDOUBLE m, VDOUBLE n);



const double stat_acceptance_level = 0.95; // ~ 2sigma  


template <class G>
void EXPECT_NORMAL_DISTRIB(double mean, double std_dev, const G& generator) {
	double r = generator();
	// compute the quantile size in muliples of sigma
	auto quantile = boost::math::quantile(boost::math::normal(0,1), 0.5 * (1+stat_acceptance_level) );
// 	cout << "Quantile of the normal distr for 95\% is " << quantile << endl;
	// Redraw more
	if (abs(r-mean) > quantile*std_dev) {
		std::cout << "Drawing more ..." << endl;
		r += generator();
		r += generator();
		r/=3;
	}
	EXPECT_LE(abs(r-mean), quantile*std_dev) << "Expected " << r << " within " << mean << " +- " << std_dev << "*"<< quantile;
}

template <class G>
void EXPECT_NORMAL_DISTRIB(Eigen::ArrayXd mean, Eigen::ArrayXd std_dev, const G& generator) {
	// In order to achive a 1-stat_acceptance_level prob. to fail we have to alter the prob. of a test to succeed
	auto single_prob = 0.5 * (1 + pow(stat_acceptance_level,1.0/mean.size()) );
	// compute the quantile size in muliples of sigma
	auto quantile = boost::math::quantile(boost::math::normal(0,1), single_prob);
// 	cout << "Quantile of the normal distr for " << single_prob << " <- " << mean.size() << " elements is " << quantile << endl;
	
	Eigen::ArrayXd r = generator();
	bool all = ((r-mean).abs() <= quantile*std_dev).all();
	// Redraw more
	if ( ! all) {
		std::cout << "Drawing more ..." << endl;
		r += generator();
		r += generator();
		r/=3;
	}
	for (int i=0; i<r.size(); i++) {
		EXPECT_LE(abs(r[i]-mean[i]), quantile*std_dev[i]) << "Failed at index " << i;
	}
}
