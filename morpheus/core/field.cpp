//
// C++ Implementation: pde_layer
//
// Description:
//
//
// Author:  <>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "field.h"
#include "focusrange.h"
#include "lattice_data_layer.cpp"
#include "xtensor/xindex_view.hpp"
#include "xtensor/xmasked_view.hpp"
#include <xtensor/xnoalias.hpp>

REGISTER_PLUGIN(Field);

template <class T >
inline auto xsub_cube(xt::xtensor<T,3> & data, const VINT& b, const VINT& t) {
	return xt::view(data,xt::range(b.z,t.z),xt::range(b.y,t.y),xt::range(b.x,t.x));
}

void Field::loadFromXML(const XMLNode node, Scope * scope) {
	Plugin::loadFromXML(node, scope);
	
	accessor = make_shared<Symbol>(this, symbol_name(), this->getDescription());
	scope->registerSymbol(accessor);
}

void Field::init(const Scope * scope) {
	if (initialized) return;
	try {
		if (initializing)
			throw string("Unable to initialize field '") + symbol_name() + "'. Detected circular dependencies in initial value.";
		initializing = true;
		auto field = make_shared<PDE_Layer>(SIM::getLattice(), SIM::getNodeLength(), false);
		field->loadFromXML(stored_node, scope);
		field->init();
		accessor->field = field;
	}
	catch (const string& e) {
		throw MorpheusException(e, stored_node);
	}
	initializing = false;
	initialized = true;
}

XMLNode Field::saveToXML() const {
	XMLNode node = accessor->field->saveToXML();
	node.updateAttribute( symbol_name().c_str(),"symbol");
	return node;
}

REGISTER_PLUGIN(VectorField);

VectorField::VectorField() : Plugin() {
	registerPluginParameter(symbol_name);
	symbol_name.setXMLPath("symbol");
	initialized = false;
	initializing = false;
}

void VectorField::loadFromXML(const XMLNode node, Scope * scope) {
	Plugin::loadFromXML(node, scope);
	
	accessor = make_shared<Symbol>(this, symbol_name(), getDescription());
	scope->registerSymbol(accessor);
}

void VectorField::init(const Scope * scope) {
	if (initialized) return;
	if (initializing)
		throw string("Unable to initialize field '") + symbol_name() + "'. Detected circular dependencies in initial value.";
	initializing = true;
	auto field = make_shared<VectorField_Layer>(SIM::getLattice(), SIM::getNodeLength());
	field->loadFromXML(stored_node, scope);
	accessor->field = field;
	field->init(scope);
	initializing = false;
	initialized = true;
}

XMLNode VectorField::saveToXML() const {
	XMLNode node = accessor->field->saveToXML();
	node.updateAttribute(symbol_name().c_str(),"symbol");
	return node;
}

// Explicit template instantiation
// template class Lattice_Data_Layer<double,1>;
// template class Lattice_Data_Layer<VDOUBLE,1>;

const float PDE_Layer::NO_VALUE = -10e6;

PDE_Layer::PDE_Layer(shared_ptr<const Lattice> l, double p_node_length, bool surface):  Lattice_Data_Layer<double,1>(l, 0.0)
{
	node_length 		= p_node_length;
	store_data 			= false;
	is_surface = surface;
	init_by_restore = false;
	initialized = false;
	
	useBuffer(true);
}


PDE_Layer::~PDE_Layer()
{/* if (write_buffer) delete[] write_buffer;*/ }

void PDE_Layer::loadFromXML(const XMLNode xNode, const Scope* scope)
{
	local_scope = scope;
	data_layer_type::loadFromXML(xNode, make_shared<ExpressionReader>(scope) );
	
	write_buffer_ad = xt::full_like(data, default_value);
	
	getXMLAttribute(xNode,"value", initial_expression);
	string symbol_name;
	getXMLAttribute(xNode, "symbol", symbol_name);
	auto val_override = scope->value_overrides().find(symbol_name);
	if ( val_override != scope->value_overrides().end()) {
		initial_expression = val_override->second;
		scope->value_overrides().erase(val_override);
	}
	else {
		// TODO :: Integrate the tiff format into the data reading / writing infrastructure
		XMLNode xTiffreader = xNode.getChildNode("TIFFReader");
		if ( ! xTiffreader.isEmpty() ) {
			shared_ptr<Plugin> p = PluginFactory::CreateInstance(string(xTiffreader.getName()));
			if(!p)
				throw MorpheusException("Unknown Initialization Plugin.", xTiffreader);
			p->loadFromXML(xTiffreader, const_cast<Scope*>(scope));
			plugins.push_back(p);
			
		}
	// 		// parse for all PDE Initializers and run them
	// 		for (int i=0; i < xInit.nChildNode(); i++) {
	// 			XMLNode xInitPDENode = xInit.getChildNode(i);
	// 			// assume its an initilizer
	// 			shared_ptr<Plugin> p = PluginFactory::CreateInstance(string(xInitPDENode.getName()));
	// 			if (!p) 
	// 				throw MorpheusException("Unknown Initialization Plugin.", xInitPDENode);
	// 			p->loadFromXML(xInitPDENode);
	// 			plugins.push_back(p);
	// 		}
	// 	}

		XMLNode xData = xNode.getChildNode("Data");
		if ( ! xData.isEmpty()) {
			restoreData(xData);
		}
	}
}

const string PDE_Layer::getXMLPath() { return ::getXMLPath(stored_node); }

void PDE_Layer::init(const SymbolFocus& focus)
{
	if (initialized) return;

	theta_y = (xt::arange<double>(0.0, l_size.y, 1.0) + 0.5)  * (M_PI/l_size.y);
	phi_coarsening.resize({uint(l_size.y)});
// 	for (uint i=0; i<l_size.y; i++) { theta_y[i] = (double(i)+0.5)  * (M_PI/l_size.y); }
	for (uint i=0; i<l_size.y; i++) { 
		phi_coarsening[i] = pow(2,floor(log(1/sin(theta_y[i]))/log(1.5)));
		phi_coarsening[i] = min(phi_coarsening[i], uint(l_size.x));
	}
	phi_coarsening[0]=l_size.x;
	phi_coarsening[l_size.y-1]=l_size.x;

	if ( !init_by_restore) {
		shared_ptr<ThreadedExpressionEvaluator<double>> init_val;
		if (initializer) {
			init_val = initializer; 
		}
		else {
			init_val = make_shared<ThreadedExpressionEvaluator<double>>(initial_expression, local_scope);
			init_val->init();
		}
		if (is_surface) {
			CPM::CELL_ID cell_id = focus.cellID();
			FocusRange range(Granularity::MembraneNode, cell_id);
			for (auto focus : range) {
				this->set(focus.membrane_pos(),init_val->safe_get(focus));
			}
		}
		else {
			multimap<FocusRangeAxis,int> r;
			if (has_reduction) {
				if (reduction == Boundary::px || reduction == Boundary::mx)
					r.insert(make_pair(FocusRangeAxis::X,0));
				if (reduction == Boundary::py || reduction == Boundary::my)
					r.insert(make_pair(FocusRangeAxis::Y,0));
				if (reduction == Boundary::pz || reduction == Boundary::mz)
					r.insert(make_pair(FocusRangeAxis::Z,0));
			}
			FocusRange range(Granularity::Node, r);
			for (auto focus : range) {
				this->set(focus.pos(),init_val->safe_get(focus));
			}
		}
	}
	for (uint i=0; i<plugins.size(); i++) {
		if ( dynamic_pointer_cast<Field_Initializer>( plugins[i] ) ) {
			dynamic_pointer_cast<Field_Initializer>( plugins[i] )->run(this);
		}
	}
	reset_boundaries();
	write_buffer = data;
	initialized = true;
}

XMLNode PDE_Layer::saveToXML() const
{
	XMLNode saved = data_layer_type::saveToXML();
	while (saved.nChildNode("Data")) {
		saved.getChildNode("Data").deleteNodeContent();
	}
	while (saved.nChildNode("TIFFReader")) {
			saved.getChildNode("TIFFReader").deleteNodeContent();
	}
	saved.addChild(storeData(""));
	return saved;
}

XMLNode PDE_Layer::storeData(string filename) const
{
	const bool to_file = ! filename.empty();
	XMLNode xNode =  XMLNode::createXMLTopNode("Data");

	if (to_file) {
		ofstream out(filename.c_str(),ios_base::trunc);
		out.setf(ios_base::scientific);
		out.precision(3);
		xNode.addAttribute("filename",filename.c_str());
		xNode.addAttribute("encoding","ascii");
		data_layer_type::storeData(out);
		out.close();
	}
	else {
		XMLParserBase64Tool encoder;
		auto origin = shadow_width;
		auto top = shadow_width + l_size;
// 		xt::xtensor<double, 3> raw_data = xt::view(Lattice_Data_Layer< double >::data, xt::range(origin.x,top.x), xt::range(origin.y,top.y), xt::range(origin.z,top.z));
		xt::xtensor<double, 3> raw_data = getData();
			
// 		cout << "Field size " << getData().size() << " " << l_size.x <<  " " << data.size() ;
		auto encoded_data = encoder.encode((unsigned char *)&(raw_data[0]), raw_data.size() * sizeof(double),true);
		xNode.addText(encoded_data);
		xNode.addAttribute("encoding","base64");
		xNode.addAttribute("word-size",to_cstr(sizeof(double)));
	}
	
	return xNode;
}



bool PDE_Layer::restoreData(const XMLNode node)
{
// 	try {
		string filename;
		if (getXMLAttribute(node, "filename",filename)) {
			ifstream in(filename.c_str());
			if (!in.is_open())
				throw string("Unable to open file: ") + filename;
			data_layer_type::restoreData(in, [] (istream& in) -> double { double t; in >> t; return t; });
			in.close();
			init_by_restore = true;
		} else {
			XMLParserBase64Tool decoder;
			int len;
			XMLError* error = nullptr;
			auto decoded = decoder.decode(node.getText(),&len,error);
			
			if (error){
				throw MorpheusException(string("Unable to load Field data:\n")+XMLNode::getError(*error),node);
			}
			if ( len != l_size.x * l_size.y * l_size.z * sizeof(double)) {
				throw MorpheusException(string("Unable to load Field data:\n")+"Wrong data size " + to_str(l_size.x * l_size.y * l_size.z * sizeof(double)) + " != " + to_str(len),node);
			}
			
			double *decoded_data = (double*) decoded;
			VINT pos;
			int i=0;
			for (pos.z=0; pos.z<l_size.z; pos.z++) {
				for (pos.y=0; pos.y<l_size.y; pos.y++) {
					for (pos.x=0; pos.x<l_size.x; pos.x++) {
						unsafe_set(pos,decoded_data[i++]);
					}
				}
			}
			reset_boundaries();
			init_by_restore = true;
		}
// 	}
// 	catch (string s) {
// 		cerr << s << endl;
// 		return false;
// 	}
	return true;
}


void PDE_Layer::write_ascii(ostream& out) const {
	string xsep(" "), ysep("\n"), zsep("\n");

	auto d = xt::flatten(data);
	valarray<float> out_data(l_size.x),out_data2(l_size.x);;
	VINT pos;
	bool is_hexagonal = (_lattice->getXMLName() == "hexagonal");

	for (pos.z=0; pos.z<l_size.z; pos.z++) {
		bool first_row = true;
		for (pos.y=0; pos.y<l_size.y; pos.y+=1) {
			pos.x=0;
			uint row_start = get_data_index(pos);
			uint row_end = row_start + l_size.x;

			// copy & convert the raw data
			for (uint i=row_start, m=0; i!=row_end; i++,m++) {
				out_data[m]=float(d[i]);
			}

			// shifting the data to map hex coordinate system
			if (is_hexagonal) {
				out_data = out_data.cshift(-pos.y/2);
				// add an interpolation step
				if (pos.y%2==1) {
					out_data2 = out_data.cshift(-1);
					out_data+= out_data2;
					out_data/=2;
				}
			}
			out << out_data[0];
			for (uint i = 1 ; i < out_data.size(); i+=1) {
				out << xsep << out_data[i];
			}
			out << ysep;

// 			if (first_row) {
// 				first_row = false;
// 				if ( ! (pos.y+y_iter)<l_size.y )
// 					pos.y-=y_iter;
// 			}
		}
		out << zsep;
	}
}

void PDE_Layer::write_ascii(ostream& out, float min_value, float max_value, int max_resolution) const {
	
	string xsep(" "), ysep(" "), zsep(" ");
	if (l_size.x>1) { ysep="\n"; zsep="\n"; }
	if (l_size.y>1) zsep="\n";

	int x_iter = 1;
	if (max_resolution) x_iter = max (1, l_size.x / max_resolution);
	int y_iter = 1;
	if (max_resolution) y_iter = max (1, l_size.y / max_resolution);
	auto d=xt::flatten(data);
	valarray<float> out_data(l_size.x), out_data2(l_size.x);;
	VINT pos;
	bool is_hexagonal = (_lattice->getXMLName() == "hexagonal");
	
	for (pos.z=0; pos.z<l_size.z; pos.z++) {
		bool first_row = true;
		for (pos.y=(y_iter)/2; pos.y<l_size.y; pos.y+=y_iter) {
			pos.x=0;
			uint row_start = get_data_index(pos);
			uint row_end = row_start + l_size.x;

			// copy & convert the raw data
			for (uint i=row_start, m=0; i!=row_end; i++,m++) {
				out_data[m] = float(d[i]);
			}

			// shifting the data to map hex coordinate system
			if (is_hexagonal) {
				out_data = out_data.cshift(-pos.y/2);
				// add an interpolation step
				if (pos.y%2==1) {
					out_data2 = out_data.cshift(-1);
					out_data+= out_data2;
					out_data/=2;
				}
			}

			// Cropping data
			if (min_value != NO_VALUE) {
				float fmin_value = min_value;
				for (uint i=row_start, m=0; i!=row_end; i++,m++) {
					if (out_data[m] < fmin_value) {
						out_data[m] = fmin_value;
					}
				}
			}
			if (max_value != NO_VALUE) {
				float fmax_value = float(max_value);
				for (uint i=row_start, m=0; i!=row_end; i++,m++) {
					if (out_data[m] > fmax_value) {
						out_data[m] = fmax_value;
					}
				}
			}

			uint i = (x_iter - 1) / 2;
			out << out_data[i];
			i+= x_iter;
			for ( ; i < out_data.size(); i+=x_iter) {
				out << xsep << out_data[i];
			}
			out << ysep;
			
			if (first_row) {
				first_row = false;
				if ( ! ((pos.y+y_iter)<l_size.y) )
					pos.y-=y_iter;
			}
		}
		out << zsep;
	}
}

void PDE_Layer::write_binary(ostream& fout, int max_resolution) {
	// This is a pure 2D (xy plane)  implementation used for gnuplot
	// For hexagonal lattices we assume periodic boundaries and map everything to a square !!
	int x_iter = 1;
	if (max_resolution) x_iter = max(1, l_size.x / max_resolution);
	int y_iter = 1;
	if (max_resolution) y_iter = max(1, l_size.y / max_resolution);
	float k( ceil((float)l_size.x / (float)x_iter) );
	fout.write((char*) &k, sizeof(float));
	uint row_length(k);
	//printf("PDE_Layer::write_binary: max_resolution = %d (x_iter = %d\tsize = %d, k = %f)\n",max_resolution, x_iter, l_size.x, k);
	auto d=xt::flatten(data);
	valarray<float> out_data(row_length), out_data2(row_length);
	for (k=0; k<l_size.x; k+=x_iter) fout.write((char*) &k, sizeof(float));

	bool is_hexagonal = 	(_lattice->getXMLName() == "hexagonal");
	VINT row_start(0,0,0);
	for (row_start.y=0; row_start.y<l_size.y; row_start.y+=y_iter) {
		uint row_index = get_data_index(row_start);
		// value of y axis
		k = _lattice -> to_orth(row_start).y;
		fout.write((char*) &k, sizeof(float));

		// copy & convert the raw data
		for (int i=row_index, m=0; i!=row_index+l_size.x; i+=x_iter,m++) {
				out_data[m]=float(d[i]);
		}

		// shifting the data to map hex coordinate system
		if (is_hexagonal) {
				out_data = out_data.cshift(-row_start.y/2);
				// add an interpolation step
				if (row_start.y%2==1) {
						out_data2 = out_data.cshift(-1);
						out_data+= out_data2;
						out_data/=2;
				}
		}

		fout.write((char*) &(out_data[0]), sizeof(float) * out_data.size());
	}

}

double PDE_Layer::sum() const
{
	if (using_domain) {
		return xt::sum(xt::filter(data, xt::equal(domain,Boundary::none)))();
// 		return data[domain == Boundary::none].sum();
	}
	return xt::sum(xt::view(data,xt::range(origin.z,top.z),xt::range(origin.y,top.y),xt::range(origin.x,top.x)))();
}

double PDE_Layer::mean() const
{
	if (using_domain) {
		auto f = xt::equal(domain,Boundary::none);
		return xt::sum(xt::filter(data,f))() / xt::sum(f)();
	}
	return sum() / (l_size.x*l_size.y*l_size.z);
}

double PDE_Layer::variance() const
{
	double average = mean();
	if (using_domain) {
		auto f = xt::equal(domain,Boundary::none);
		return xt::sum(xt::pow(xt::filter(data,f)-average,2))()/ (xt::sum(f)() -1);
	}
	return xt::sum( xt::pow(xt::view(data,xt::range(origin.z,top.z),xt::range(origin.y,top.y),xt::range(origin.x,top.x))-average, 2) )() / (l_size.x*l_size.y*l_size.z-1);
}


double PDE_Layer::min_val() const {
	if (using_domain) {
		return xt::amin(xt::filter(data,xt::equal(domain,Boundary::none)))();
	}
	return xt::amin(xt::view(data,xt::range(origin.z,top.z),xt::range(origin.y,top.y),xt::range(origin.x,top.x)))();
}

double PDE_Layer::max_val() const {
	if (using_domain) {
		return xt::amax(xt::filter(data,xt::equal(domain,Boundary::none)))();
	}
	return xt::amax(xt::view(data,xt::range(origin.z,top.z),xt::range(origin.y,top.y),xt::range(origin.x,top.x)))();
}


VectorField_Layer::VectorField_Layer(shared_ptr<const Lattice> lattice, double node_length) :
	Lattice_Data_Layer<VDOUBLE,1>(lattice,VDOUBLE(0,0,0),""), node_length(node_length)
{
	initial_expression = "";
	init_by_restore = false;
	useBuffer(true);
}

void VectorField_Layer::loadFromXML(XMLNode node, const Scope* scope)
{
	data_layer_type::loadFromXML(node, make_shared<ExpressionReader>(scope));
	getXMLAttribute(node,"value", initial_expression);
	
	XMLNode xData = node.getChildNode("Data");
	if ( ! xData.isEmpty()) {
		restoreData(xData);
	}
}


void VectorField_Layer::init(const Scope* scope) {
	
	if (!initial_expression.empty() && !init_by_restore) {
		ExpressionEvaluator<VDOUBLE> init_val(initial_expression, scope);
		init_val.init();
		FocusRange range(Granularity::Node, scope);
		for (auto focus : range) {
			this->set(focus.pos(),init_val.get(focus));
		}
	}
}


XMLNode VectorField_Layer::saveToXML() const
{
	XMLNode saved = data_layer_type::saveToXML();
	while (saved.nChildNode("Data")) {
		saved.getChildNode("Data").deleteNodeContent();
	}
	saved.addChild(storeData(""));
	return saved;
}


XMLNode VectorField_Layer::storeData(string filename) const
{
	const bool to_file = ! filename.empty();
	XMLNode xNode =  XMLNode::createXMLTopNode("Data");

	if (to_file) {
		ofstream out(filename.c_str(),ios_base::trunc);
		out.setf(ios_base::scientific);
		out.precision(3);
		xNode.addAttribute("filename",filename.c_str());
		xNode.addAttribute("encoding","ascii");
		Lattice_Data_Layer< VDOUBLE,1 >::storeData(out);
		out.close();
	}
	else {
		XMLParserBase64Tool encoder;
		auto d = getData();
		valarray<VDOUBLE> data(d.size());
		uint i=0;
		for (const auto& val : d) { data[i++] = val; }
// 		cout << "Field size " << getData().size() << " " << l_size.x <<  " " << data.size() ;
		auto encoded_data = encoder.encode((unsigned char *)&(data[0]), data.size() * sizeof(VDOUBLE),true);
		xNode.addText(encoded_data);
		xNode.addAttribute("encoding","base64");
		xNode.addAttribute("word-size",to_cstr(sizeof(VDOUBLE)));
	}
	
	return xNode;
}



bool VectorField_Layer::restoreData(const XMLNode node)
{
	string filename;
	if (getXMLAttribute(node, "filename",filename)) {
		ifstream in(filename.c_str());
		if (!in.is_open())
			throw string("Unable to open file: ") + filename;
		data_layer_type::restoreData(in, [] (istream& in) -> VDOUBLE { VDOUBLE t; in >> t; return t; });
		in.close();
		init_by_restore = true;
	} else {
		XMLParserBase64Tool decoder;
		int len;
		XMLError* error = nullptr;
		auto decoded = decoder.decode(node.getText(),&len,error);
		
		if (error){
			throw MorpheusException(string("Unable to load VectorField data:\n")+XMLNode::getError(*error),node);
		}
		if ( len != l_size.x * l_size.y * l_size.z * sizeof(VDOUBLE)) {
			throw MorpheusException(string("Unable to load VectorField data:\n")+"Wrong data size " + to_str(l_size.x * l_size.y * l_size.z * sizeof(VDOUBLE)) + " != " + to_str(len),node);
		}
		
		VDOUBLE *decoded_data = (VDOUBLE*) decoded;
		VINT pos;
		int i=0;
		for (pos.z=0; pos.z<l_size.z; pos.z++) {
			for (pos.y=0; pos.y<l_size.y; pos.y++) {
				for (pos.x=0; pos.x<l_size.x; pos.x++) {
					unsafe_set(pos, decoded_data[i++]);
				}
			}
		}
		reset_boundaries();
		init_by_restore = true;
	}
	return true;
}

