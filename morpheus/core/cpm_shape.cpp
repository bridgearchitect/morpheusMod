#include "cpm_shape.h"
// #include <map>

CPMShape::BoundaryScalingMode CPMShape::scalingMode = BoundaryScalingMode::Magno;
Neighborhood CPMShape::boundary_neighborhood = Neighborhood();

double CPMShape::BoundaryLengthScaling(const Neighborhood& neighborhood, CPMShape::BoundaryScalingMode mode)
{
	map<Lattice::Structure, vector<double> > magno_scaling_by_order =
	{ { Lattice::Structure::linear,    {0,1,2,3,4} },
	  { Lattice::Structure::square,    {0,1,3,5,11,15,18,26,36} },
	  { Lattice::Structure::hexagonal, {0,2.206,6.003,10.41,22.04,28.63,36.2,52.04,60.82} },
	  { Lattice::Structure::hexagonal_new, {0,2.206,6.003,10.41,22.04,28.63,36.2,52.04,60.82} },
	  { Lattice::Structure::cubic,     {0,1,5,9,11,23,39,47,70} }
	};
	
	double scaling = 1;
// 	BoundaryScalingMode mode = BoundaryScalingMode::Magno;
	switch (scalingMode) {
		case BoundaryScalingMode::Magno :
		{
			const auto& scalings = magno_scaling_by_order[neighborhood.lattice().getStructure()];
			if (neighborhood.order() >= scalings.size()) {
				throw string("No neighborhood normalization scaling available for order "+to_str(neighborhood.order()));
			}
			scaling = scalings[neighborhood.order()];
			break;
		}
		case BoundaryScalingMode::NeighborNumber :
			scaling = neighborhood.size();
			break;
		case BoundaryScalingMode::None:
		case BoundaryScalingMode::Classic:
			scaling = 1.0;
			break;
	}
	return scaling;
}
